#pragma once
#ifndef _D3DH_
#define _D3DH_

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#include <d3dx9.h>
#include <stdio.h>

extern ID3DXLine* p_Line;

#define Red       D3DCOLOR_ARGB(255, 255, 000, 000)
#define Green     D3DCOLOR_ARGB(255, 127, 255, 000)
#define Orange    D3DCOLOR_ARGB(255, 255, 140, 000)
#define Blue      D3DCOLOR_ARGB(255, 000, 000, 255)
#define Yellow    D3DCOLOR_ARGB(255, 255, 255,  51)
#define Black     D3DCOLOR_ARGB(255, 000, 000, 000)
#define Grey      D3DCOLOR_ARGB(255, 112, 112, 112)
#define DGrey     D3DCOLOR_ARGB(255, 060, 060, 060)
#define Gold      D3DCOLOR_ARGB(255, 255, 215, 000)
#define Pink      D3DCOLOR_ARGB(255, 255, 192, 203)
#define Purple    D3DCOLOR_ARGB(255, 128, 000, 128)
#define White     D3DCOLOR_ARGB(255, 255, 255, 249)
#define Cyan      D3DCOLOR_ARGB(255, 000, 120, 134)
#define Magenta   D3DCOLOR_ARGB(255, 255, 000, 255)
#define TBlack    D3DCOLOR_ARGB(128, 000, 000, 000)
#define TGrey	  D3DCOLOR_ARGB(120, 112, 112, 112)

void FillRGB(float x, float y, float w, float h, int r, int g, int b, int a);

void DrawText(int X, int Y, D3DCOLOR Cor, ID3DXFont *font, const char* sNome, ...);

void DrawLine(float x, float y, float xx, float yy, int r, int g, int b, int a);

void DrawBox(float x, float y, float width, float height, float px, int r, int g, int b, int a);

void DrawCenterLine(float x, float y, int w, int r, int g, int b, int Width, int Height);

RECT GetTextSize(const char *szText, ID3DXFont *pFont);

void DrawRect(IDirect3DDevice9* Unidade, int baseX, int baseY, int baseW, int baseH, D3DCOLOR Cor);

void DrawBoxedText(IDirect3DDevice9* pDevice, int centered, int x, int y, D3DCOLOR c, ID3DXFont *font, const char* text, ...);

#endif //_D3DH_