#pragma once
#ifndef _STRUCTS_H
#define _STRUCTS_H

#include "Vector.h"
#include "Basic.h"
#include "BitBuf.h"
#include "CRC.h"
#include "CBaseHandle.h"
#include "Weapon_List.h"

#define streql(x,y) strcmp( (x) , (y) ) == 0


typedef float matrix3x4[3][4];
#pragma region weird shit

#define BONE_USED_BY_HITBOX 0x00000100
#define BONE_USED_BY_ANYTHING		0x0007FF00
#define MAXSTUDIOBONES		128

typedef struct player_info_s
{
	char			name[32];
	int				userID;
	char			guid[33];
	unsigned long	friendsID;
	char			friendsName[32];
	bool			fakeplayer;
	bool			ishltv;
	unsigned long	customFiles[4];
	unsigned char	filesDownloaded;
} player_info_t;

template< typename Function > Function call_vfunc(PVOID Base, DWORD Index)
{
	PDWORD* VTablePointer = (PDWORD*)Base;
	PDWORD VTableFunctionBase = *VTablePointer;
	DWORD dwAddress = VTableFunctionBase[Index];
	return (Function)(dwAddress);
}

class ClientClass
{
private:
	BYTE _chPadding[8];
public:
	char* chName;
	void * Table;
	ClientClass* pNextClass;
	int iClassID;
};

class CUserCmd
{
public:
	virtual ~CUserCmd() {}; //Destructor 0
	int command_number; //4
	int tick_count; //8
	Vector viewangles; //C
	//Vector aimdirection;
	float forwardmove; //18
	float sidemove; //1Cs
	float upmove; //20
	int	buttons; //24
	byte impulse; //28
	int weaponselect; //2C
	int weaponsubtype; //30
	int random_seed; //34
	short mousedx; //38
	short mousedy; //3A
	bool hasbeenpredicted; //3C;
	//Vector headangles;
	//Vector headoffset;
	//3D + 0x92 = 0xCF
	CRC32_t GetChecksum(void) const
	{
		CRC32_t crc;

		CRC32_Init(&crc);
		CRC32_ProcessBuffer(&crc, &command_number, sizeof(command_number));
		CRC32_ProcessBuffer(&crc, &tick_count, sizeof(tick_count));
		CRC32_ProcessBuffer(&crc, &viewangles, sizeof(viewangles));
		//CRC32_ProcessBuffer(&crc, &aimdirection, sizeof(aimdirection));
		CRC32_ProcessBuffer(&crc, &forwardmove, sizeof(forwardmove));
		CRC32_ProcessBuffer(&crc, &sidemove, sizeof(sidemove));
		CRC32_ProcessBuffer(&crc, &upmove, sizeof(upmove));
		CRC32_ProcessBuffer(&crc, &buttons, sizeof(buttons));
		CRC32_ProcessBuffer(&crc, &impulse, sizeof(impulse));
		CRC32_ProcessBuffer(&crc, &weaponselect, sizeof(weaponselect));
		CRC32_ProcessBuffer(&crc, &weaponsubtype, sizeof(weaponsubtype));
		CRC32_ProcessBuffer(&crc, &random_seed, sizeof(random_seed));
		CRC32_ProcessBuffer(&crc, &mousedx, sizeof(mousedx));
		CRC32_ProcessBuffer(&crc, &mousedy, sizeof(mousedy));
		//CRC32_ProcessBuffer( &crc, &hasbeenpredicted, sizeof( hasbeenpredicted ) );
		//CRC32_ProcessBuffer( &crc, &headangles, sizeof( headangles ) );
		//CRC32_ProcessBuffer( &crc, &headoffset, sizeof( headoffset ) );        
		CRC32_Final(&crc);

		return crc;
	}
};

class CVerifiedUserCmd
{
public:
	CUserCmd        m_cmd;
	CRC32_t         m_crc;
};

class CInput
{
public:
	virtual		void		Init_All(void);
	virtual		void		Shutdown_All(void);
	virtual		int			GetButtonBits(int);
	virtual		void		CreateMove(int sequence_number, float input_sample_frametime, bool active);
	virtual		void		ExtraMouseSample(float frametime, bool active);
	virtual		bool		WriteUsercmdDeltaToBuffer( /*bf_write*/int *buf, int from, int to, bool isnewcommand);
	virtual		void		EncodeUserCmdToBuffer( /*bf_write&*/ int buf, int slot);
	virtual		void		DecodeUserCmdFromBuffer( /*bf_read&*/int buf, int slot);
	CUserCmd* GetUserCmd(int seq)
	{
		typedef CUserCmd* (__thiscall* OriginalFn)(PVOID, int);
		return call_vfunc<OriginalFn>(this, 8)(this, seq);
	}

	class CVerifiedUserCmd
	{
	public:
		CUserCmd        m_cmd;
		CRC32_t         m_crc;
	};

	CUserCmd         *m_pCommands;
	CVerifiedUserCmd *m_pVerifiedCommands;
};

#pragma region ClientModeShared
class IClientModeShared{
public:
	void CreateMove(float flInputSampleTime, CUserCmd * pCommand){
		typedef void(__thiscall* oCreateMove)(PVOID, float, CUserCmd*);
		return call_vfunc<oCreateMove>(this, 19)(this, flInputSampleTime, pCommand);
	}
};
#pragma endregion

#pragma endregion

class IBaseClientDLL
{
public:
	ClientClass * GetAllClasses(){
		typedef ClientClass*(__thiscall* oGetAllClasses)(PVOID);
		return call_vfunc< oGetAllClasses >(this, 8)(this);
	}
	void CreateMove(int sequence_number, float frametime, bool active)
	{
		typedef void(__thiscall* oMove)(PVOID, int, float, bool);
		return call_vfunc< oMove >(this, 21)(this, sequence_number, frametime, active);
	}
	bool DispatchUserMessage(int msg_type, bf_read &msg_data){
		typedef bool(__thiscall* oDispatchUserMessage)(PVOID, int,bf_read&);
		return call_vfunc<oDispatchUserMessage>(this, 36)(this, msg_type, msg_data);
	}
};

extern IBaseClientDLL* tfBaseClient;


enum TF2CLASSES{
	TF_SCOUT = 1,
	TF_SNIPER,
	TF_SOLDIER,
	TF_DEMO, 
	TF_MEDIC,
	TF_HEAVY,
	TF_PYRO,
	TF_SPY,
	TF_ENGI
};

#pragma region CBaseEntity
class CBaseEntity
{
public:
	///m_nPlayerCond
	int GetCond(void)
	{
		return *(int*)(this + 0x5B0);
	}

	//m_hActiveWeapon
	CBaseHandle GetActiveWeaponHandle(void)
	{
		CBaseHandle weaponHandle = *(CBaseHandle*)(this + 0xDB0);
		return weaponHandle;
	}

	bool SetupBones(matrix3x4 *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef bool(__thiscall* oSetupBones)(PVOID, matrix3x4*, int, int, float);
		return call_vfunc< oSetupBones>(pRenderable, 16)(pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
	}

	void GetBonePosition(int iBone, Vector &origin, float time)
	{
		//studiohdr_t *pStudioHdr = g_pModelInfo->GetStudiomodel(GetModel());

		matrix3x4 bonetoworld[128];

		if (!this->SetupBones(bonetoworld, MAXSTUDIOBONES, BONE_USED_BY_HITBOX, time))
			return;

		float x = bonetoworld[iBone][0][3];
		float y = bonetoworld[iBone][1][3];
		float z = bonetoworld[iBone][2][3];

		origin = Vector(x, y, z);
	}
	Vector& GetAbsOrigin()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 9)(this); //Subtracted 3?
	}
	Vector& GetAbsAngles()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 10)(this);
	}
	void GetWorldSpaceCenter(Vector& vWorldSpaceCenter)
	{
		Vector vMin, vMax;
		this->GetRenderBounds(vMin, vMax);
		vWorldSpaceCenter = this->GetAbsOrigin();
		vWorldSpaceCenter.z += (vMin.z + vMax.z) / 2;
	}
	DWORD* GetModel()
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef DWORD* (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(pRenderable, 9)(pRenderable);
	}

	ClientClass* GetClientClass()
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef ClientClass* (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(pNetworkable, 2)(pNetworkable);
	}
	bool IsDormant()
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(pNetworkable, 8)(pNetworkable);
	}
	int GetIndex()
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef int(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(pNetworkable, 9)(pNetworkable);
	}
	void GetRenderBounds(Vector& mins, Vector& maxs)
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef void(__thiscall* OriginalFn)(PVOID, Vector&, Vector&);
		call_vfunc<OriginalFn>(pRenderable, 20)(pRenderable, mins, maxs);
	}
	//m_bIsAlive
	bool isAlive(){
		return !*(bool*)(this + 0xA5);
	}
	//m_iMaxHealth or Something
	int GetMaxHealth(){
		return *(int*)(this + 0x1790);
	}
	//m_iHealth
	int GetHealth(){
		return *(int*)(this + 0xA8);
	}
	//m_iTeamNum
	int GetTeamNumber(){
		return *(int*)(this + 0xB0);
	}

	DWORD GetBoneMatrix(){
		return *(DWORD*)(this + 0x5AC);
	}

	Vector GetVelocity(){
		return *(Vector*)(this + 0x120);
	}
	//m_bIsGlowing
	bool IsGlowing(){
		return *(bool*)(this + 0xDB4);
	}

	unsigned int *GetGlowIndex(){
		return *(unsigned int**)(this + 0xDB8);
	}

	void UpdateGlow(bool bEnabled)
	{
		if (*((bool*)(this + (DWORD)0x0DB4/*m_bGlowEnabled*/)) == bEnabled)
			return;

		*((bool*)(this + (DWORD)0x0DB4/*m_bGlowEnabled*/)) = bEnabled;

		typedef void(__thiscall* UpdateGlowEffect_t)(CBaseEntity* pBaseEnt);
		((UpdateGlowEffect_t)(*(PDWORD*)this)[225])(this);
	}
	//Have to find with reclass
	float GetCloak(){
		return *(float*)(this + 0x2064);
	}

	int GetServerTime(){
		return *(int*)(this + 0x1138);
	}

	// If Greater than 0, they are disguised. Number will be team.
	// Find in reclass
	int GetDisgsuisedTeam(){
		return *(int*)(this + 0x788);
	}
	// Find in Reclass
	int GetClass(){
		return *(int*)(this + 0x153C);
	}

	const char * GetTFClassName(){
		switch (GetClass()){
		case TF_SCOUT:
			return "Scout";
		case TF_SNIPER:
			return "Sniper";
		case TF_SOLDIER:
			return "Soldier";
		case TF_DEMO:
			return "Demoman";
		case TF_MEDIC:
			return "Medic";
		case TF_HEAVY:
			return "Heavy";
		case TF_PYRO:
			return "Pyro";
		case TF_SPY:
			return "Spy";
		case TF_ENGI:
			return "Engineer";
		default:
			return "Unknown_Class";
		}
	}
	//m_fFlags
	int GetFlags()
	{
		return *(DWORD*)(this + 0x37C);
	}


};
#pragma endregion

#pragma region C_BaseCombatWeapon
class CBaseCombatWeapon : public CBaseEntity{
public:
	float GetNextPrimaryAttackTime()
	{
		return *(float*)(this + 0x8B0);
	}

	bool ReadyToBackstab(){
		return *(bool*)(this + 0xBC8);
	}

	char const * GetName(){
		typedef char const*(__thiscall* oName)(PVOID);
		return call_vfunc< oName >(this, 394)(this); //MB 394
	}
};
#pragma endregion

#pragma region IClient Stuff
class IClientNetworkable;
class IClientRenderable;
class IClientEntity;
class IClientThinkable;

class IClientUnknown
{
public:
	virtual void*           GetCollideable();
	virtual IClientNetworkable*     GetClientNetworkable();
	virtual IClientRenderable*      GetClientRenderable();
	virtual IClientEntity*          GetIClientEntity();
	virtual IClientEntity*          GetBaseEntity();
	virtual IClientThinkable*       GetClientThinkable();
};
class IClientRenderable
{
public:
	virtual void*                                   GetIClientUnknown();
	virtual Vector const&                   GetRenderOrigin(void);
	virtual Vector const&                   GetRenderAngles(void);
	virtual bool                                    ShouldDraw(void);
	virtual bool                                    IsTransparent(void);
	virtual bool                                    UsesPowerOfTwoFrameBufferTexture();
	virtual bool                                    UsesFullFrameBufferTexture();
	virtual void                                    GetShadowHandle() const;
	virtual void*                                   RenderHandle();
	virtual const void*							   GetModel() const;
	virtual int                                             DrawModel(int flags);
	virtual int                                             GetBody();
	virtual void                                    ComputeFxBlend();

	bool SetupBones(matrix3x4 *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
	{
		typedef bool(__thiscall* oSetupBones)(PVOID, matrix3x4*, int, int, float);
		return call_vfunc< oSetupBones>(this, 13)(this, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
	}


};
class IClientNetworkable
{
public:
	virtual IClientUnknown* GetIClientUnknown();
	virtual void                    Release();
	virtual void*                   GetClientClass();// FOR NETVARS FIND YOURSELF ClientClass* stuffs
	virtual void                    NotifyShouldTransmit( /* ShouldTransmitState_t state*/);
	virtual void                    OnPreDataChanged( /*DataUpdateType_t updateType*/);
	virtual void                    OnDataChanged( /*DataUpdateType_t updateType*/);
	virtual void                    PreDataUpdate( /*DataUpdateType_t updateType*/);
	virtual void                    PostDataUpdate( /*DataUpdateType_t updateType*/);
	virtual void                    unknown();
	virtual bool                    IsDormant(void);
	virtual int                             Index(void) const;
	virtual void                    ReceiveMessage(int classID /*, bf_read &msg*/);
	virtual void*                   GetDataTableBasePtr();
	virtual void                    SetDestroyedOnRecreateEntities(void);
};
class IClientThinkable
{
public:
	virtual IClientUnknown*         GetIClientUnknown();
	virtual void                            ClientThink();
	virtual void*                           GetThinkHandle();
	virtual void                            SetThinkHandle(void* hThink);
	virtual void                            Release();
};
class __declspec (novtable)IClientEntity : public IClientUnknown, public IClientRenderable, public IClientNetworkable, public IClientThinkable
{
public:
public:
	virtual void                    Release(void);
	virtual void                    blahblahpad(void);
	virtual Vector& GetAbsOrigin(void) const;//in broken place use GetOrigin Below
	virtual const Vector&   GetAbsAngles(void) const;

	Vector GetOrigin()
	{
		return *reinterpret_cast< Vector* >((DWORD)this + (DWORD)0x134);
	}

	int GetTeamNum()
	{
		return *reinterpret_cast< int* >((DWORD)this + (DWORD)0xF0);
	}
	int GetFlags()
	{
		return *reinterpret_cast< int* >((DWORD)this + (DWORD)0x100);
	}

	DWORD GetCrosshairTargetAddress()
	{
		return (DWORD)this + (DWORD)0x2EF8; //not sure
	}


};
class CEntList
{
public:

	CBaseEntity* GetClientEntity(int entnum)
	{
		typedef CBaseEntity* (__thiscall* OriginalFn)(PVOID, int);
		return call_vfunc<OriginalFn>(this, 3)(this, entnum);
	}
	CBaseEntity* GetClientEntityFromHandle(CBaseHandle hEnt)
	{
		typedef CBaseEntity* (__thiscall* OriginalFn)(PVOID, CBaseHandle);
		return call_vfunc<OriginalFn>(this, 4)(this, hEnt);
	}
	int GetHighestEntityIndex(void)
	{
		typedef int(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 6)(this);
	}
	CBaseCombatWeapon * GetWeaponFromHandle(CBaseHandle hEnt){
		return (CBaseCombatWeapon*)GetClientEntityFromHandle(hEnt);
	}
};

extern CEntList * tfCEntList;


class IClientEntityList
{
public:
	virtual void Function0();
	virtual void Function1();
	virtual void Function2();
	virtual IClientEntity *         GetClientEntity(int entnum);
	virtual IClientEntity *         GetClientEntityFromHandle(CBaseHandle hEnt);
	virtual int                                     NumberOfEntities(bool bIncludeNonNetworkable);
	virtual int                                     GetHighestEntityIndex(void);
	virtual void                            SetMaxEntities(int maxents);
	virtual int                                     GetMaxEntities();
};
#pragma endregion 

#pragma region  Ray Things

struct Ray_t
{
	__declspec(align(16)) Vector   m_Start;
	__declspec(align(16)) Vector   m_Delta;
	__declspec(align(16)) Vector   m_StartOffset;
	__declspec(align(16)) Vector   m_Extents;

	const   matrix3x4* m_pWorldAxisTransform;

	bool    m_IsRay;
	bool    m_IsSwept;

	Ray_t() : m_pWorldAxisTransform(NULL) { }

	void Init(Vector& vecStart, Vector& vecEnd)
	{
		m_Delta = vecEnd - vecStart;

		m_IsSwept = (m_Delta.LengthSqr() != 0);

		m_Extents.x = m_Extents.y = m_Extents.z = 0.0f;

		m_pWorldAxisTransform = NULL;

		m_IsRay = true;

		m_StartOffset.x = m_StartOffset.y = m_StartOffset.z = 0.0f;

		m_Start = vecStart;
	}
};

struct cplane_t
{
	Vector  normal;
	float   dist;
	BYTE    type;
	BYTE    signbits;
	BYTE    pad[2];
};

class CBaseTrace
{
public:
	Vector                  startpos;
	Vector                  endpos;
	cplane_t                plane;
	float                   fraction;
	int                     contents;
	unsigned short			dispFlags;
	bool                    allsolid;
	bool                    startsolid;
};

struct csurface_t
{
	const char*             name;
	short                   surfaceProps;
	unsigned short  flags;
};

class CGameTrace : public CBaseTrace
{
public:
	bool                    DidHitWorld() const;
	bool                    DidHitNonWorldEntity() const;
	int                             GetEntityIndex() const;
	bool                    DidHit() const;
public:
	float                   fractionleftsolid;
	csurface_t              surface;
	int                             hitgroup;
	short                   physicsbone;
	//unsigned short  worldSurfaceIndex;
	IClientEntity*               m_pEnt;
	int                             hitbox;
};

inline bool CGameTrace::DidHit() const
{
	return fraction < 1.0f || allsolid || startsolid;
}

typedef CGameTrace trace_t;

enum TraceType_t
{
	TRACE_EVERYTHING = 0,
	TRACE_WORLD_ONLY,
	TRACE_ENTITIES_ONLY,
	TRACE_EVERYTHING_FILTER_PROPS,
};


class ITraceFilter
{
public:
	virtual bool ShouldHitEntity(IClientEntity *pEntity, int contentsMask) = 0;
	virtual TraceType_t    GetTraceType() const = 0;
};

class CTraceFilter : public ITraceFilter
{
public:
	CTraceFilter(IClientEntity* pPassEntity)
	{
		m_pPassEnt = pPassEntity;
	}

	bool ShouldHitEntity(IClientEntity* pHandleEntity, int contentsMask)
	{
		return !(pHandleEntity == m_pPassEnt);
	}

	virtual TraceType_t GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}

private:
	IClientEntity* m_pPassEnt;
};
#pragma endregion

#pragma region IEngine Stuff
class IEngineTrace
{
public:
	void TraceRay(const Ray_t &ray, unsigned int fMask, CTraceFilter *pTraceFilter, trace_t *pTrace)
	{
		typedef void(__thiscall* vTraceRay)(PVOID, const Ray_t&, unsigned int, CTraceFilter*, trace_t*);
		return call_vfunc<vTraceRay>(this, 4)(this, ray, fMask, pTraceFilter, pTrace);
	}
};
class IEngineClient
{
public:
	bool GetPlayerInfo(int ent_num, player_info_t *pinfo)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID, int, player_info_t *);
		return call_vfunc<OriginalFn>(this, 8)(this, ent_num, pinfo);
	}
	void GetScreenSize(int& width, int& height)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int&, int&);
		return call_vfunc<OriginalFn>(this, 5)(this, width, height);
	}
	bool Con_IsVisible(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 11)(this);
	}
	int GetLocalPlayer(void)
	{
		typedef int(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 12)(this);
	}
	float Time(void)
	{
		typedef float(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 14)(this);
	}
	void GetViewAngles(Vector& va)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, Vector& va);
		return call_vfunc<OriginalFn>(this, 19)(this, va);
	}
	void SetViewAngles(Vector& va)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, Vector& va);
		return call_vfunc<OriginalFn>(this, 20)(this, va);
	}
	int GetMaxClients(void)
	{
		typedef int(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 21)(this);
	}
	bool IsInGame(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 26)(this);
	}
	bool IsConnected(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 27)(this);
	}
	bool IsDrawingLoadingImage(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 28)(this);
	}
	const matrix3x4& WorldToScreenMatrix(void)
	{
		typedef const matrix3x4& (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 36)(this);
	}
	bool IsTakingScreenshot(void)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 85)(this);
	}
	DWORD* GetNetChannelInfo(void)
	{
		typedef DWORD* (__thiscall* OriginalFn)(PVOID);
		return call_vfunc<OriginalFn>(this, 72)(this);
	}
	void ClientCmd_Uned(const char* chCommandString)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const char *);
		return call_vfunc<OriginalFn>(this, 106)(this, chCommandString);
	}
};
#pragma endregion 

#pragma region MASKS AND CONTENTS
#define	CONTENTS_EMPTY			0		// No contents

#define	CONTENTS_SOLID			0x1		// an eye is never valid in a solid
#define	CONTENTS_WINDOW			0x2		// translucent, but not watery (glass)
#define	CONTENTS_AUX			0x4
#define	CONTENTS_GRATE			0x8		// alpha-tested "grate" textures.  Bullets/sight pass through, but solids don't
#define	CONTENTS_SLIME			0x10
#define	CONTENTS_WATER			0x20
#define	CONTENTS_BLOCKLOS		0x40	// block AI line of sight
#define CONTENTS_OPAQUE			0x80	// things that cannot be seen through (may be non-solid though)
#define	LAST_VISIBLE_CONTENTS	CONTENTS_OPAQUE

#define ALL_VISIBLE_CONTENTS (LAST_VISIBLE_CONTENTS | (LAST_VISIBLE_CONTENTS-1))

#define CONTENTS_TESTFOGVOLUME	0x100
#define CONTENTS_UNUSED			0x200	

// unused 
// NOTE: If it's visible, grab from the top + update LAST_VISIBLE_CONTENTS
// if not visible, then grab from the bottom.
// CONTENTS_OPAQUE + SURF_NODRAW count as CONTENTS_OPAQUE (shadow-casting toolsblocklight textures)
#define CONTENTS_BLOCKLIGHT		0x400

#define CONTENTS_TEAM1			0x800	// per team contents used to differentiate collisions 
#define CONTENTS_TEAM2			0x1000	// between players and objects on different teams

// ignore CONTENTS_OPAQUE on surfaces that have SURF_NODRAW
#define CONTENTS_IGNORE_NODRAW_OPAQUE	0x2000

// hits entities which are MOVETYPE_PUSH (doors, plats, etc.)
#define CONTENTS_MOVEABLE		0x4000

// remaining contents are non-visible, and don't eat brushes
#define	CONTENTS_AREAPORTAL		0x8000

#define	CONTENTS_PLAYERCLIP		0x10000
#define	CONTENTS_MONSTERCLIP	0x20000

// currents can be added to any other contents, and may be mixed
#define	CONTENTS_CURRENT_0		0x40000
#define	CONTENTS_CURRENT_90		0x80000
#define	CONTENTS_CURRENT_180	0x100000
#define	CONTENTS_CURRENT_270	0x200000
#define	CONTENTS_CURRENT_UP		0x400000
#define	CONTENTS_CURRENT_DOWN	0x800000

#define	CONTENTS_ORIGIN			0x1000000	// removed before bsping an entity

#define	CONTENTS_MONSTER		0x2000000	// should never be on a brush, only in game
#define	CONTENTS_DEBRIS			0x4000000
#define	CONTENTS_DETAIL			0x8000000	// brushes to be added after vis leafs
#define	CONTENTS_TRANSLUCENT	0x10000000	// auto set if any surface has trans
#define	CONTENTS_LADDER			0x20000000
#define CONTENTS_HITBOX			0x40000000	// use accurate hitboxes on trace

// NOTE: These are stored in a short in the engine now.  Don't use more than 16 bits
#define	SURF_LIGHT		0x0001		// value will hold the light strength
#define	SURF_SKY2D		0x0002		// don't draw, indicates we should skylight + draw 2d sky but not draw the 3D skybox
#define	SURF_SKY		0x0004		// don't draw, but add to skybox
#define	SURF_WARP		0x0008		// turbulent water warp
#define	SURF_TRANS		0x0010
#define SURF_NOPORTAL	0x0020	// the surface can not have a portal placed on it
#define	SURF_TRIGGER	0x0040	// FIXME: This is an xbox hack to work around elimination of trigger surfaces, which breaks occluders
#define	SURF_NODRAW		0x0080	// don't bother referencing the texture

#define	SURF_HINT		0x0100	// make a primary bsp splitter

#define	SURF_SKIP		0x0200	// completely ignore, allowing non-closed brushes
#define SURF_NOLIGHT	0x0400	// Don't calculate light
#define SURF_BUMPLIGHT	0x0800	// calculate three lightmaps for the surface for bumpmapping
#define SURF_NOSHADOWS	0x1000	// Don't receive shadows
#define SURF_NODECALS	0x2000	// Don't receive decals
#define SURF_NOPAINT	SURF_NODECALS	// the surface can not have paint placed on it
#define SURF_NOCHOP		0x4000	// Don't subdivide patches on this surface 
#define SURF_HITBOX		0x8000	// surface is part of a hitbox

#define	MASK_ALL					(0xFFFFFFFF)
// everything that is normally solid
#define	MASK_SOLID					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// everything that blocks player movement
#define	MASK_PLAYERSOLID			(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_PLAYERCLIP|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// blocks npc movement
#define	MASK_NPCSOLID				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTERCLIP|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// blocks fluid movement
#define	MASK_NPCFLUID				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTERCLIP|CONTENTS_WINDOW|CONTENTS_MONSTER)
// water physics in these contents
#define	MASK_WATER					(CONTENTS_WATER|CONTENTS_MOVEABLE|CONTENTS_SLIME)
// everything that blocks lighting
#define	MASK_OPAQUE					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_OPAQUE)
// everything that blocks lighting, but with monsters added.
#define MASK_OPAQUE_AND_NPCS		(MASK_OPAQUE|CONTENTS_MONSTER)
// everything that blocks line of sight for AI
#define MASK_BLOCKLOS				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_BLOCKLOS)
// everything that blocks line of sight for AI plus NPCs
#define MASK_BLOCKLOS_AND_NPCS		(MASK_BLOCKLOS|CONTENTS_MONSTER)
// everything that blocks line of sight for players
#define	MASK_VISIBLE					(MASK_OPAQUE|CONTENTS_IGNORE_NODRAW_OPAQUE)
// everything that blocks line of sight for players, but with monsters added.
#define MASK_VISIBLE_AND_NPCS		(MASK_OPAQUE_AND_NPCS|CONTENTS_IGNORE_NODRAW_OPAQUE)
// bullets see these as solid
#define	MASK_SHOT					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTER|CONTENTS_WINDOW|CONTENTS_DEBRIS|CONTENTS_HITBOX)
// bullets see these as solid, except monsters (world+brush only)
#define MASK_SHOT_BRUSHONLY			(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_DEBRIS)
// non-raycasted weapons see this as solid (includes grates)
#define MASK_SHOT_HULL				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTER|CONTENTS_WINDOW|CONTENTS_DEBRIS|CONTENTS_GRATE)
// hits solids (not grates) and passes through everything else
#define MASK_SHOT_PORTAL			(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTER)
// everything normally solid, except monsters (world+brush only)
#define MASK_SOLID_BRUSHONLY		(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_GRATE)
// everything normally solid for player movement, except monsters (world+brush only)
#define MASK_PLAYERSOLID_BRUSHONLY	(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_PLAYERCLIP|CONTENTS_GRATE)
// everything normally solid for npc movement, except monsters (world+brush only)
#define MASK_NPCSOLID_BRUSHONLY		(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTERCLIP|CONTENTS_GRATE)
// just the world, used for route rebuilding
#define MASK_NPCWORLDSTATIC			(CONTENTS_SOLID|CONTENTS_WINDOW|CONTENTS_MONSTERCLIP|CONTENTS_GRATE)
// just the world, used for route rebuilding
#define MASK_NPCWORLDSTATIC_FLUID	(CONTENTS_SOLID|CONTENTS_WINDOW|CONTENTS_MONSTERCLIP)
// These are things that can split areaportals
#define MASK_SPLITAREAPORTAL		(CONTENTS_WATER|CONTENTS_SLIME)

// UNDONE: This is untested, any moving water
#define MASK_CURRENT				(CONTENTS_CURRENT_0|CONTENTS_CURRENT_90|CONTENTS_CURRENT_180|CONTENTS_CURRENT_270|CONTENTS_CURRENT_UP|CONTENTS_CURRENT_DOWN)

// everything that blocks corpse movement
// UNDONE: Not used yet / may be deleted
#define	MASK_DEADSOLID				(CONTENTS_SOLID|CONTENTS_PLAYERCLIP|CONTENTS_WINDOW|CONTENTS_GRATE)

#define MASK_AIMBOT 0x200400B
#pragma endregion

enum playercontrols
{
	IN_ATTACK =		(1 << 0),
	IN_JUMP =		(1 << 1),
	IN_DUCK =		(1 << 2),
	IN_FORWARD =	(1 << 3),
	IN_BACK =		(1 << 4),
	IN_USE =		(1 << 5),
	IN_CANCEL =		(1 << 6),
	IN_LEFT =		(1 << 7),
	IN_RIGHT =		(1 << 8),
	IN_MOVELEFT =	(1 << 9),
	IN_MOVERIGHT =	(1 << 10),
	IN_ATTACK2 =	(1 << 11),
	IN_RUN =		(1 << 12),
	IN_RELOAD =		(1 << 13),
	IN_ALT1 =		(1 << 14),
	IN_ALT2 =		(1 << 15),
	IN_SCORE =		(1 << 16),	// Used by client.dll for when scoreboard is held down
	IN_SPEED =		(1 << 17),	// Player is holding the speed key
	IN_WALK =		(1 << 18),	// Player holding walk key
	IN_ZOOM =		(1 << 19),	// Zoom key for HUD zoom
	IN_WEAPON1 =	(1 << 20),	// weapon defines these bits
	IN_WEAPON2 =	(1 << 21),	// weapon defines these bits
	IN_BULLRUSH =	(1 << 22),
};

#pragma region Conditions
enum TFCond
{
	TFCond_Slowed = (1 << 0), //Toggled when a player is slower than normal.
	TFCond_Zoomed = (1 << 1), //Toggled when a player is zoomed in.
	TFCond_Disguising = (1 << 2), //Toggled when a Spy is disguising. 
	TFCond_Disguised = (1 << 3), //Toggled when a Spy is disguised.
	TFCond_Cloaked = (1 << 4), //Toggled when a Spy is invisible.
	TFCond_Ubercharged = (1 << 5), //Toggled when a player is ‹berCharged.
	TFCond_TeleportedGlow = (1 << 6), //Will activate when someone leaves a teleporter and has glow beneath their feet.
	TFCond_Taunting = (1 << 7), //Activates when a player is taunting.
	TFCond_UberchargeFading = (1 << 8), //Activates when the ‹berCharge is fading.
	TFCond_CloakFlicker = (1 << 9), //When a normal cloak Spy gets bumped into, or a CloakAndDagger spy with no energy is moving.
	TFCond_Teleporting = (1 << 10), //Only activates for a brief second when a player is riding a teleporter; not very useful.
	TFCond_Kritzkrieged = (1 << 11), //When a player has a crit buff from the KrRitzkrieg. (No longer used?)
	TFCond_TmpDamageBonus = (1 << 12), //Unknown what this is for.
	TFCond_DeadRingered = (1 << 13), //Toggled when the player is under reduced damage from the Deadringer.
	TFCond_Bonked = (1 << 14), //Player is under the effects of Bonk! Atomic Punch.
	TFCond_Stunned = (1 << 15), //Player was stunned from a Sandman ball.
	TFCond_Buffed = (1 << 16), //Toggled when a player is within a Buff Banner's range.
	TFCond_Charging = (1 << 17), //Toggled when a Demo Knight charges with the shield.
	TFCond_DemoBuff = (1 << 18), //Toggled when a Demo Knight has heads from the Eyelander.
	TFCond_CritCola = (1 << 19), //Toggled when the player is under the effect of Crit-a-Cola.
	TFCond_InHealRadius = (1 << 20), //Unknown what this is for.
	TFCond_Healing = (1 << 21), //Toggled when someone is being healed by a medic or a dispenser.
	TFCond_OnFire = (1 << 22), //Toggled when a player is on fire.
	TFCond_Overhealed = (1 << 23), //Toggled when a player has >100% health.
	TFCond_Jarated = (1 << 24), //Toggled when a player is hit with a sniper's Jarate.
	TFCond_Bleeding = (1 << 25), //Toggled from Boston Basher/Tribalman's Shiv/Southern Hospitality damage.
	TFCond_DefenseBuffed = (1 << 26), //Toggled when a player is within a Battalion's Backup's range.
	TFCond_Milked = (1 << 27), //Player was hit with a jar of Mad Milk.
	TFCond_MegaHeal = (1 << 28), //Player is under the effect of Quick-Fix charge.
	TFCond_RegenBuffed = (1 << 29), //Toggled when a player is within a Concheror's range.
	TFCond_MarkedForDeath = (1 << 30), //Player is marked for death by a Fan O'War hit. Effects are similar to TFCond_Jarated.

	TFCondEx_SpeedBuffAlly = (1 << 0), //Toggled when a player gets hit with the disciplinary action.
#ifdef HALLOWEEN
	TFCondEx_HalloweenCritCandy = (1 << 1), //Only for Scream Fortress event maps that drop crit candy.
#endif
	TFCondEx_CritHype = (1 << 4), //Soda Popper crits.
	TFCondEx_CritOnFirstBlood = (1 << 5), //Arena first blood crit buff.
	TFCondEx_CritOnWin = (1 << 6), //End of round crits.
	TFCondEx_CritOnFlagCapture = (1 << 7), //CTF intelligence capture crits.
	TFCondEx_CritOnKill = (1 << 8), //Unknown what this is for.
	TFCondEx_RestrictToMelee = (1 << 9), //Unknown what this is for.
	TFCondEx_PyroCrits = (1 << 12), //Pyro is getting crits from the Mmmph charge.
	TFCondEx_PyroHeal = (1 << 13), //Pyro is being healed from the Mmmph charge and can not be damaged.

	TFCond_Crits = (TFCond_Kritzkrieged),
	TFCond_MiniCrits = (TFCond_Buffed | TFCond_CritCola),
	TFCondEx_Crits = (
#ifdef HALLOWEEN
	TFCondEx_HalloweenCritCandy |
#endif
	TFCondEx_CritOnFirstBlood | TFCondEx_CritOnWin | TFCondEx_CritOnFlagCapture | TFCondEx_CritOnKill | TFCondEx_PyroCrits),
	TFCondEx_MiniCrits = (TFCondEx_CritHype),
	TFCond_IgnoreStates = (TFCond_Ubercharged | TFCond_Bonked),
	TFCondEx_IgnoreStates = (TFCondEx_PyroHeal)
};
#pragma endregion

#define SPY_KNIFE (WPN_Knife|WPN_NewKnife|WPN_EternalReward |WPN_Kunai  | WPN_BigEarner  | WPN_WangaPrick  | WPN_SharpDresser  | WPN_Spycicle  | WPN_FestiveKnife  | WPN_BlackRose  | WPN_BotKnifeS  | WPN_BotKnifeG  | WPN_BotKnifeR  | WPN_BotKnifeB  | WPN_BotKnifeC  | WPN_BotKnifeD  | WPN_BotKnifeES  | WPN_BotKnifeEG )

#pragma region Player Flags
#define	FL_ONGROUND				(1<<0)	// At rest / on the ground
#define FL_DUCKING				(1<<1)	// Player flag -- Player is fully crouched
#define FL_ANIMDUCKING			(1<<2)	// Player flag -- Player is in the process of crouching or uncrouching but could be in transition
// examples:                                   Fully ducked:  FL_DUCKING &  FL_ANIMDUCKING
//           Previously fully ducked, unducking in progress:  FL_DUCKING & !FL_ANIMDUCKING
//                                           Fully unducked: !FL_DUCKING & !FL_ANIMDUCKING
//           Previously fully unducked, ducking in progress: !FL_DUCKING &  FL_ANIMDUCKING
#define	FL_WATERJUMP			(1<<3)	// player jumping out of water
#define FL_ONTRAIN				(1<<4) // Player is _controlling_ a train, so movement commands should be ignored on client during prediction.
#define FL_INRAIN				(1<<5)	// Indicates the entity is standing in rain
#define FL_FROZEN				(1<<6) // Player is frozen for 3rd person camera
#define FL_ATCONTROLS			(1<<7) // Player can't move, but keeps key inputs for controlling another entity
#define	FL_CLIENT				(1<<8)	// Is a player
#define FL_FAKECLIENT			(1<<9)	// Fake client, simulated server side; don't send network messages to them
// NON-PLAYER SPECIFIC (i.e., not used by GameMovement or the client .dll ) -- Can still be applied to players, though
#define	FL_INWATER				(1<<10)	// In water

// NOTE if you move things up, make sure to change this value
#define PLAYER_FLAG_BITS		11

#define	FL_FLY					(1<<11)	// Changes the SV_Movestep() behavior to not need to be on ground
#define	FL_SWIM					(1<<12)	// Changes the SV_Movestep() behavior to not need to be on ground (but stay in water)
#define	FL_CONVEYOR				(1<<13)
#define	FL_NPC					(1<<14)
#define	FL_GODMODE				(1<<15)
#define	FL_NOTARGET				(1<<16)
#define	FL_AIMTARGET			(1<<17)	// set if the crosshair needs to aim onto the entity
#define	FL_PARTIALGROUND		(1<<18)	// not all corners are valid
#define FL_STATICPROP			(1<<19)	// Eetsa static prop!		
#define FL_GRAPHED				(1<<20) // worldgraph has this ent listed as something that blocks a connection
#define FL_GRENADE				(1<<21)
#define FL_STEPMOVEMENT			(1<<22)	// Changes the SV_Movestep() behavior to not do any processing
#define FL_DONTTOUCH			(1<<23)	// Doesn't generate touch functions, generates Untouch() for anything it was touching when this flag was set
#define FL_BASEVELOCITY			(1<<24)	// Base velocity has been applied this frame (used to convert base velocity into momentum)
#define FL_WORLDBRUSH			(1<<25)	// Not moveable/removeable brush entity (really part of the world, but represented as an entity for transparency or something)
#define FL_OBJECT				(1<<26) // Terrible name. This is an object that NPCs should see. Missiles, for example.
#define FL_KILLME				(1<<27)	// This entity is marked for death -- will be freed by game DLL
#define FL_ONFIRE				(1<<28)	// You know...
#define FL_DISSOLVING			(1<<29) // We're dissolving!
#define FL_TRANSRAGDOLL			(1<<30) // In the process of turning into a client side ragdoll.
#define FL_UNBLOCKABLE_BY_PLAYER (1<<31) // pusher that can't be blocked by the player
#pragma endregion

#endif