#pragma once
#include <Windows.h>
#include "Basic.h"
#include "Structs.h"
#include "Steam\steam_api.h"
#

void CalcAngle(const Vector& vSource, const Vector& vDest, Vector& vAngles);

void NormalizeVector(Vector &angle);

extern ISteamFriends * tfSteamFriends;

void ChangeName(char* name);