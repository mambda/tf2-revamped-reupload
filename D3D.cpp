#include "D3D.h"

void FillRGB(float x, float y, float w, float h, int r, int g, int b, int a)
{
	D3DXVECTOR2 vLine[2];

	p_Line->SetWidth(w);

	vLine[0].x = x + w / 2;
	vLine[0].y = y;
	vLine[1].x = x + w / 2;
	vLine[1].y = y + h;

	p_Line->Begin();
	p_Line->Draw(vLine, 2, D3DCOLOR_RGBA(r, g, b, a));
	p_Line->End();
}


void DrawText(int X, int Y, D3DCOLOR Cor, ID3DXFont *font, const char* sNome, ...)
{
	char	sTexto[1024] = "";
	va_list	ap;

	if (!sNome || *sNome == '\0')
		return;

	va_start(ap, sNome);
	_vsnprintf_s(sTexto, 1024, 1023, sNome, ap);
	va_end(ap);

	RECT Posicao = { X, Y, X + 500, Y + 50 };
	font->DrawText(NULL, sTexto, -1, &Posicao, DT_LEFT | DT_WORDBREAK, Cor);
}

void DrawLine(float x, float y, float xx, float yy, int r, int g, int b, int a)
{
	D3DXVECTOR2 dLine[2];

	p_Line->SetWidth(1);

	dLine[0].x = x;
	dLine[0].y = y;

	dLine[1].x = xx;
	dLine[1].y = yy;

	p_Line->Draw(dLine, 2, D3DCOLOR_ARGB(a, r, g, b));

}

void DrawBox(float x, float y, float width, float height, float px, int r, int g, int b, int a)
{
	D3DXVECTOR2 points[5];
	points[0] = D3DXVECTOR2(x, y);
	points[1] = D3DXVECTOR2(x + width, y);
	points[2] = D3DXVECTOR2(x + width, y + height);
	points[3] = D3DXVECTOR2(x, y + height);
	points[4] = D3DXVECTOR2(x, y);
	p_Line->SetWidth(px);
	p_Line->Draw(points, 5, D3DCOLOR_RGBA(r, g, b, a));
}

void DrawCenterLine(float x, float y, int w, int r, int g, int b, int Width, int Height)
{
	D3DXVECTOR2 dPoints[2];
	dPoints[0] = D3DXVECTOR2(x, y);
	dPoints[1] = D3DXVECTOR2(Width / 2, Height);
	p_Line->SetWidth(w);
	p_Line->Draw(dPoints, 2, D3DCOLOR_RGBA(r, g, b, 255));
}

RECT GetTextSize(const char *szText, ID3DXFont *pFont)
{
	RECT rcRect = { 0, 0, 0, 0 };
	if (pFont)
	{
		// calculate required rect
		pFont->DrawText(NULL, szText, strlen(szText), &rcRect, DT_CALCRECT,
			D3DCOLOR_XRGB(0, 0, 0));
	}

	return rcRect;
}

void DrawRect(IDirect3DDevice9* Unidade, int baseX, int baseY, int baseW, int baseH, D3DCOLOR Cor)
{
	D3DRECT BarRect = { baseX, baseY, baseX + baseW, baseY + baseH };
	Unidade->Clear(1, &BarRect, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, Cor, 0, 0);
}

void DrawBoxedText(IDirect3DDevice9* pDevice, int centered, int x, int y, D3DCOLOR c, ID3DXFont *font, const char* text, ...){
	RECT textBounds = GetTextSize(text, font);
	if (centered == 1)
		x -= (textBounds.right - textBounds.left) / 2 + 3;
	if (centered == 2)
		x -= (textBounds.right - textBounds.left) + 5;
	//BlackBox
	DrawRect(pDevice, x, y, (textBounds.right - textBounds.left) + 4, textBounds.bottom - textBounds.top + 4, TBlack);

	//Not Black Box
	DrawRect(pDevice, x, y, (textBounds.right - textBounds.left) + 4, 1, c);
	DrawRect(pDevice, x, y, 1, textBounds.bottom - textBounds.top + 4, c);
	DrawRect(pDevice, x + (textBounds.right - textBounds.left) + 4, y, 1, textBounds.bottom - textBounds.top + 4, c);
	DrawRect(pDevice, x, y + textBounds.bottom - textBounds.top + 4, (textBounds.right - textBounds.left) + 4, 1, c);

	//Text
	DrawText(x + 2, y + 2, c, font, text);
}
