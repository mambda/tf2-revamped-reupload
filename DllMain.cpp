﻿#include "Utility.h"
#include <detours.h>
#include "Structs.h"
#include "Weapon_List.h"
#include "Hacks.h"
#include <vector>
#include "VMTHooks.h"

#define GetLPlayer tfCEntList->GetClientEntity(tfEngineClient->GetLocalPlayer())
#define DEBUG_TRACE FALSE
#define DEBUG_BOOLS FALSE
#define DEBUG_CONSOLE FALSE
#define DEBUG_PLAYER FALSE
#define DEBUG_PLAYER_ADDY FALSE
#define DEBUG_BONES FALSE

HANDLE nameThread = NULL;

const char * firstName = NULL;
char currName[50] = "";
std::vector<char *> stolenNames;

void stealNames();
void GetTarget();

#pragma region Typedefs
HMODULE ourModule = NULL;
typedef HRESULT(WINAPI* EndScene_)(LPDIRECT3DDEVICE9 pDevice);
EndScene_ oEndScene;

typedef HRESULT(WINAPI* Reset_t)(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters);
Reset_t oReset;

typedef void(__thiscall* tCreateMove)(PVOID, float, CUserCmd*);
tCreateMove oCreateMove;

typedef bool(__thiscall* tDispatchUserMessage)(PVOID, int, bf_read&);
tDispatchUserMessage oDispatchUserMessage;

typedef void* (*CreateInterfaceFn)(const char *name, int *iret);

#pragma endregion

#define XASSERT( x ) if( !x ) MessageBoxW( 0, L#x, 0, 0 );

#pragma region Globals
bool bOnce = false;
bool forcedOff = false;
LPD3DXFONT font14pt, font12pt;
DWORD EndSceneaddy = NULL;
DWORD DispatchUserAddy = NULL;
DWORD resetAddy = NULL;
bool walling = false, aimbot = false, stealing = false, shoot = false, silentAim = false;

IBaseClientDLL * tfBaseClient;
IEngineTrace * tfEngineTrace;
IClientEntityList * tfEntList;
IClientEntity * moi;
CEntList * tfCEntList;
ISteamClient*		tfSteamClient = NULL;
CInput * tfInput;
IClientModeShared * tfClientMode;
#pragma endregion

#pragma region External Variables
ID3DXLine* p_Line;
//
int Width = 0;
int Height = 0;
//
CBaseEntity * lockedPlayer;
//
ISteamFriends * tfSteamFriends;
IEngineClient * tfEngineClient;
//
CVMTHookManager* pDispatchUserMessageHook;
CVMTHookManager* pCreateMoveHook;
//Related
PDWORD* m_ppdwClassBase;
DWORD m_dwVMTSize;
////	/////

#pragma endregion

#pragma region Useless Things
bool WorldToScreen(Vector &vOrigin, Vector &vScreen)
{
	if (Width == 0 || Height == 0)
	{
		return false;
	}


	const matrix3x4& worldToScreen = tfEngineClient->WorldToScreenMatrix(); //Grab the world to screen matrix from CEngineClient::WorldToScreenMatrix

	float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3]; //Calculate the angle in compareson to the player's camera.
	vScreen.z = 0; //Screen doesn't have a 3rd dimension.


	if (w > 0.001) //If the object is within view.
	{
		float fl1DBw = 1 / w; //Divide 1 by the angle.
		vScreen.x = (Width / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * Width + 0.5); //Get the X dimension and push it in to the Vector.
		vScreen.y = (Height / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * Height + 0.5); //Get the Y dimension and push it in to the Vector.
		return true;
	}

	return false;
}

void resetAngles();


void checkKeys(){
	if (GetAsyncKeyState(VK_HOME) & 1){
		silentAim = !silentAim;
	}
	if (GetAsyncKeyState(VK_INSERT) & 1){
		walling = !walling;
	}
	if (GetAsyncKeyState(VK_LSHIFT) < 0){
		aimbot = true;
	}
	else
	{
		if (lockedPlayer != NULL)
			lockedPlayer = NULL;
		aimbot = false;
	}
	if (GetAsyncKeyState(VK_PRIOR) & 1){//Page Up
		stealing = !stealing;
		if (stealing){
			nameThread = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)stealNames, NULL, NULL, NULL);
		}
		else
		{
			TerminateThread(nameThread, 0);
		}
	}
	if (GetAsyncKeyState(VK_NEXT) & 1){
		//Page Down
		shoot = !shoot;
	}
}

bool isPlayer(CBaseEntity * ent){
	if (!strncmp(ent->GetClientClass()->chName, "CTFPlayer", 9) && ent->GetClientClass()->chName[9] != 'R')
		return true;
	return false;
}

bool isAlive(CBaseEntity * ent){
	if (ent->GetHealth() > 1 && ent->isAlive()){
		return true;
	}
	return false;
}

Vector GetBodyPos(CBaseEntity * ent){
	Vector out;
	if (ent->GetClass() == TF_ENGI){
		ent->GetBonePosition(4, out, tfEngineClient->Time());
	}
	else{
		ent->GetBonePosition(2, out, tfEngineClient->Time());
	}
	return out;
}

Vector GetHeadPos(CBaseEntity * ent){
	Vector out;
	if (ent->GetClass() == TF_DEMO){
		ent->GetBonePosition(16, out, tfEngineClient->Time());
	}
	else if (ent->GetClass() == TF_ENGI){
		ent->GetBonePosition(8, out, tfEngineClient->Time());
	}
	else{
		ent->GetBonePosition(6, out, tfEngineClient->Time());
	}
	return out;
}

Vector GetPos(CBaseEntity * ent)
{
	return ent->GetAbsOrigin();
}
#pragma endregion

#pragma region Trace Things
bool isVisible(CBaseEntity * ent){
	if (ent == NULL || !isPlayer(ent))
		return false;

	CTraceFilter filter(moi);
	

	Ray_t ray;
	Vector bonePosEnemy, bonePosMe;

	bonePosEnemy = GetHeadPos(ent);
	bonePosMe = GetHeadPos(GetLPlayer);
	
	ray.Init(bonePosMe, bonePosEnemy);
	trace_t trace;

	if (tfEngineTrace != NULL)
	{
		tfEngineTrace->TraceRay(ray, MASK_SHOT_BRUSHONLY, &filter, &trace);

		if (trace.allsolid)
			return false;

		if (trace.fraction < 0.89f) //SOMETHING IS BLOCKING IT.
		{
#if DEBUG_TRACE
			Vector start, end;
			if (!WorldToScreen(trace.startpos, start) || !WorldToScreen(trace.endpos, end))
				return false;
			DrawLine(start.x, start.y, end.x, end.y, 255, 255, 255, 255);
#endif
			return false;
		}
		else // NO BLOCK.
		{
#if DEBUG_TRACE
			Vector start, end;
			if (!WorldToScreen(trace.startpos, start) || !WorldToScreen(trace.endpos, end))
				return true;
			DrawLine(start.x, start.y, end.x, end.y, 0, 0, 0, 255);
#endif
			return true;
		}
	}

	return false;
}
#pragma endregion

#pragma region Hacks

#pragma region ESP

void DrawVertHealthBar(int x, int y, int health, int maxHealth, int w, int h, int r, int g,int b)
{
	if (health > maxHealth)
		maxHealth = health;
	x -= w / 2;//Center x
	FillRGB(x, y, w + 1, h, 0, 0, 0, 255);
	UINT hw = (UINT)(((h - 2) * health) / maxHealth);//Get height of health bar
	FillRGB(x + 1, y - 1, w - 1, hw, r, g, b, 255);//Draw health
}

void removeGlow(){
	for (int i = 1; i < tfEntList->GetHighestEntityIndex(); i++){

		CBaseEntity *pBaseEntity = tfCEntList->GetClientEntity(i);

		if (pBaseEntity == NULL || !isPlayer(pBaseEntity) || pBaseEntity == GetLPlayer || !isAlive(pBaseEntity))
			continue;

		pBaseEntity->UpdateGlow(false);
	}
}

void SetupBones(CBaseEntity * ent){
	matrix3x4 MatrixArray[128];

	float fTime = tfEngineClient->Time();
	if (fTime < 0.001f)
		return;

	if (!ent->SetupBones(&MatrixArray[0], 128, 0x00000100, fTime))
		return;
}

void ESP(){
	for (int i = 1; i < tfEntList->GetHighestEntityIndex(); i++){
		
		CBaseEntity *pBaseEntity = tfCEntList->GetClientEntity(i);

		if (pBaseEntity == NULL || !isPlayer(pBaseEntity) || !isAlive(pBaseEntity))
			continue;

#if !DEBUG_PLAYER && !DEBUG_PLAYER_ADDY // IF WE ARE DEBUGGING THE PLAYER, ALLOW US TO BE A TARGET.
		if (pBaseEntity == GetLPlayer)
			continue;
#endif
// 		if (pBaseEntity->IsDormant())
// 			continue;

		Vector entHead = GetHeadPos(pBaseEntity);
		Vector entFeet = GetPos(pBaseEntity);
		Vector vecHead,vecFeet;

		if (WorldToScreen(entHead, vecHead)){
		
			if (WorldToScreen(entFeet, vecFeet)){
				Vector realPos;
				float flWidth, flHeight;
				flHeight = vecFeet.y - vecHead.y;
				flWidth = ((vecFeet.y - vecHead.y) / 3) * 2;
				realPos = Vector(vecHead.x - (flWidth / 2), vecHead.y, 0);

				DrawVertHealthBar(realPos.x - 10, realPos.y, pBaseEntity->GetHealth(), pBaseEntity->GetMaxHealth(), 3, flHeight, 0, 255, 0);

				if (pBaseEntity->GetClass() == TF_SPY)
				{
					DrawVertHealthBar(realPos.x + flWidth + 5, realPos.y, pBaseEntity->GetCloak(), 100, 3, flHeight, 000, 120, 134); // Cloak Meter.
				}

#if DEBUG_PLAYER_ADDY
				char addy[50];
				CBaseCombatWeapon* myWep = tfCEntList->GetWeaponFromHandle(pBaseEntity->GetActiveWeaponHandle());
				sprintf_s(addy, "%X", (DWORD)myWep);
				DrawText(realPos.x, realPos.y, Gold, font14pt, addy);
#endif

#if DEBUG_BONES
				for (int i = 0; i < 16; i++){
					Vector bonePos, boneScreen;
					pBaseEntity->GetBonePosition(i,bonePos,tfEngineClient->Time());
					if (WorldToScreen(bonePos, boneScreen)){
						DrawText(boneScreen.x, boneScreen.y, Gold, font14pt, "%d", i);
					}
				}
#endif

				if (pBaseEntity->GetTeamNumber() == GetLPlayer->GetTeamNumber()){

					char name[33];
					player_info_t pInfo;
					tfEngineClient->GetPlayerInfo(i, &pInfo);
					sprintf_s(name, pInfo.name);

					DrawText(realPos.x + flWidth + 10, realPos.y + (flHeight / 2 - 20), Cyan, font14pt, name);

					if (isVisible(pBaseEntity))
					{
						DrawBox(realPos.x, realPos.y, flWidth, flHeight, 2, 0, 255, 0, 255);
					}
					else
					{
						DrawBox(realPos.x, realPos.y, flWidth, flHeight, 2, 000, 120, 134, 255);
					}
					continue;
				}

				else
				{

					char name[33];
					player_info_t pInfo;
					tfEngineClient->GetPlayerInfo(i, &pInfo);
					sprintf_s(name, pInfo.name);
					DrawText(realPos.x + flWidth + 10, realPos.y + (flHeight / 2 - 20), Red, font14pt, name);
					DrawText(realPos.x + flWidth + 10, realPos.y + (flHeight / 2), Red, font14pt, "Class: %s", pBaseEntity->GetTFClassName());

					if (pBaseEntity->IsGlowing() == false){
						pBaseEntity->UpdateGlow(true);
					}

					if (isVisible(pBaseEntity)){
						DrawBox(realPos.x, realPos.y, flWidth, flHeight, 2, 255, 0, 0, 255);
					}
					else
					{
						DrawBox(realPos.x, realPos.y, flWidth, flHeight, 2, 255, 140, 000, 255);
					}
				}

				if (pBaseEntity == lockedPlayer){
					DrawBox(realPos.x, realPos.y, flWidth, flHeight, 2, 255, 255, 249, 255);
				}
			}
		}
	}
}
#pragma endregion

#pragma region Aimbot

void Aimbot(CBaseEntity * ent, CUserCmd * pCommand){
	Vector targetHead, AimAngles, bone;
	CBaseEntity*me = GetLPlayer;
	if (me->GetClass() != TF_HEAVY && me->GetClass() != TF_SCOUT){
		CalcAngle(GetHeadPos(me), GetHeadPos(ent), AimAngles);
	}
	else
	{
		CalcAngle(GetHeadPos(me), GetBodyPos(ent), AimAngles);
	}
	NormalizeVector(AimAngles);
	pCommand->viewangles = AimAngles;
	tfEngineClient->SetViewAngles(AimAngles);
	if (shoot)
		pCommand->buttons |= IN_ATTACK;
	
}

bool isUbered(CBaseEntity*ent){
	if (ent->GetCond() & TFCond_Ubercharged)
		return true;

	return false;
}

void GetTarget(CUserCmd * pCommand){
	//Are we already locked onto a player?
check:
	if (lockedPlayer != NULL){
		//Now lets check him!
		if (!isAlive(lockedPlayer) || !isVisible(lockedPlayer) || !isPlayer(lockedPlayer) || isUbered(lockedPlayer) /*|| lockedPlayer->IsDormant()*/)
		{
			lockedPlayer = NULL; //We don't want him anymore!
			//BAIL OUT.
			goto check;
		}

		//Not horrible? Great!
		Aimbot(lockedPlayer, pCommand);
	}
	else
	{
		//Okay, we need to get one!
		for (int i = 1; i < tfCEntList->GetHighestEntityIndex(); i++){
			CBaseEntity * testEnt = tfCEntList->GetClientEntity(i);
			if (testEnt == NULL || !isAlive(testEnt) || testEnt == GetLPlayer || !isPlayer(testEnt))
				continue;

			//Not null or dead... nor me.
			if (testEnt->GetTeamNumber() == GetLPlayer->GetTeamNumber())
				continue;
			//Darn, was on our team.

			//Is it visible?!?!?
			if (!isVisible(testEnt))
				continue;

			//BUT IS IT A PLAYER?
			if (!isPlayer(testEnt))
				continue;

			// Is it on our screen?!?!
			Vector throwaway;
			if (!WorldToScreen(GetHeadPos(testEnt), throwaway))
				continue;

			//ARE THEY UBERED?
			if (isUbered(testEnt))
				continue;

// 			if (testEnt->IsDormant())
// 				continue;

			//Alright, use it!
			lockedPlayer = testEnt;
			goto check;
		}
	}
}

void __stdcall hkCreateMove(float flInputSampleTime, CUserCmd* pCommand)
{
	if (!tfEngineClient->IsInGame())
		return oCreateMove(tfClientMode, flInputSampleTime, pCommand);;

	CBaseEntity * pBaseEntity = GetLPlayer;

	if (pBaseEntity == NULL)
		return oCreateMove(tfClientMode, flInputSampleTime, pCommand);;

	oCreateMove(tfClientMode, flInputSampleTime, pCommand);

	if (pCommand){
		Vector oldView = pCommand->viewangles;
		float oldSideMove = pCommand->sidemove;
		float oldForwardMove = pCommand->forwardmove;
		if (aimbot && isAlive(pBaseEntity)){
			GetTarget(pCommand);
		}

		if (isAlive(pBaseEntity)){
			//Auto Knife
			CBaseCombatWeapon* myWep = tfCEntList->GetWeaponFromHandle(pBaseEntity->GetActiveWeaponHandle());
			if (streql(myWep->GetClientClass()->chName, "CTFKnife")){
				if (myWep->ReadyToBackstab()){
					pCommand->buttons |= IN_ATTACK;
				}
			}

			//Auto BHOP
			if (pCommand->buttons & IN_JUMP)
			{
				int iFlags = pBaseEntity->GetFlags();

				if (!(iFlags &FL_ONGROUND))
					pCommand->buttons &= ~IN_JUMP;
			}
		}
	}

	return oCreateMove(tfClientMode, flInputSampleTime, pCommand);
}

#pragma endregion

#pragma region Name Stealer
int failedTimes = 0;
void stealNames(){
	char name[32] = { 0 };
	char realName[32] = { 0 };
	
	for (int i = 1; i < tfEntList->GetHighestEntityIndex(); i++){
		CBaseEntity * ent = tfCEntList->GetClientEntity(i);
		if (ent == NULL || !isPlayer(ent) || ent == GetLPlayer)
			continue;

		//Not me, Is player.
		player_info_t pInfo;
		tfEngineClient->GetPlayerInfo(i, &pInfo);

		//Check List.
		strcpy(name, pInfo.name);
		strcat(name, ".");

		strcpy(realName, tfSteamFriends->GetPersonaName());

		if (!strcmp(realName, name))
			continue;

		if (std::find(stolenNames.begin(), stolenNames.end(), name) != stolenNames.end()){
			//We found it and it wasn't at the push back spot!
			//Must mean we've used it before!
			//Try again!
			failedTimes++;
			continue;
		}

		if (failedTimes >= 10){
			//So we've failed 10 times to get a new name..
			//Probably means we've tried all the names, so let's do them over.
			stolenNames.clear();
		}

		strcpy(currName, name);
		tfSteamFriends->SetPersonaName(name);
		stolenNames.push_back(name);

		Sleep(100000);
	}
	stealNames();
}
#pragma endregion

#pragma region UserMessageDispatch

bool __stdcall hkDispatchUserMessage(int msg_type, bf_read& msg_data){
	bool ret = oDispatchUserMessage(tfBaseClient, msg_type, msg_data);

	//NOICE.
	switch (msg_type)
	{
	case 45:
		char issue[50], player[50];
		int team = msg_data.ReadByte();
		int initiator = msg_data.ReadByte();
		msg_data.ReadString(issue, 50);
		msg_data.ReadString(player, 50);
		if (!strncmp(issue, "#TF_vote_kick_player_", 21) && (!strcmp(player, tfSteamFriends->GetPersonaName()) || !strcmp(player,firstName))){
			//We gotta restart!
			tfEngineClient->ClientCmd_Uned("retry");
		}
		break;
	}
	
	//Return Original function
	return ret;
}

#pragma endregion

#pragma endregion

#pragma region EndScene + HookSteam
void hookSteam(){
	HMODULE hmSteam = GetModuleHandle("SteamClient.dll");
	typedef void* (*CreateInterfaceFn)(const char *name, int *iret);
	CreateInterfaceFn steamClientFactory = (CreateInterfaceFn)GetProcAddress(hmSteam, "CreateInterface");

	if (!tfSteamClient){
		tfSteamClient = (ISteamClient*)steamClientFactory(STEAMCLIENT_INTERFACE_VERSION, NULL);
	}
	HSteamPipe hNewPipe = tfSteamClient->CreateSteamPipe();
	HSteamUser hNewUser = tfSteamClient->ConnectToGlobalUser(hNewPipe);

	if (!tfSteamFriends)
	{
		tfSteamFriends = tfSteamClient->GetISteamFriends(hNewUser, hNewPipe, STEAMFRIENDS_INTERFACE_VERSION);
		firstName = tfSteamFriends->GetPersonaName();
	}
}


HRESULT WINAPI hkEndScene(LPDIRECT3DDEVICE9 pDevice)
{
	//_asm NOP;
	HRESULT hRet = oEndScene(pDevice);
	if (!bOnce){
		bOnce = true;
		D3DXCreateFont(pDevice, 14, 0, FW_NORMAL, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Tahoma", &font14pt);
		D3DXCreateFont(pDevice, 12, 0, FW_NORMAL, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Tahoma", &font12pt);
		D3DXCreateLine(pDevice, &p_Line);
		hookSteam();
		firstName = tfSteamFriends->GetPersonaName();
	}
	DrawBoxedText(pDevice, 0,2, 2, Purple, font14pt, "Team Fortress 2: Revamped");
	if(stealing){
		DrawBoxedText(pDevice, 0, 2, 50, Purple, font14pt,currName);
	}

	if (Width == 0 || Height == 0)
	{
		D3DVIEWPORT9 ViewPort;
		pDevice->GetViewport(&ViewPort);
		Width = ViewPort.Width;
		Height = ViewPort.Height;
	}

	checkKeys();
	if (tfEngineClient->IsInGame()){
		if (moi == NULL){
			moi = tfEntList->GetClientEntity(tfEngineClient->GetLocalPlayer());
		}
		if (stealing){
			//stealNames();
		}
		else
		{
			if (stolenNames.size() > 0){
				stolenNames.clear();
			}
		}
		if (walling){
			ESP();
		}
		else
		{
			removeGlow();
		}
	}
	return hRet;
}
#pragma endregion

#pragma region Startup Stuff
HRESULT WINAPI hkReset(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	//_asm NOP;

	if (font14pt)
		font14pt->OnLostDevice();
	if (font12pt)
		font12pt->OnLostDevice();
	HRESULT hRet = oReset(pDevice, pPresentationParameters);

	if (hRet == D3D_OK) {

		if (font14pt)
			font14pt->OnLostDevice();
		if (font12pt)
			font12pt->OnLostDevice();
	}

	return hRet;
}

void hookSource(){
	CreateInterfaceFn clientBase;
	CreateInterfaceFn engineBase;

	engineBase = (CreateInterfaceFn)GetProcAddress(GetModuleHandle("engine.dll"), "CreateInterface");

	tfEngineTrace = (IEngineTrace*)engineBase("EngineTraceClient003", NULL);
	tfEngineClient = (IEngineClient*)engineBase("VEngineClient013", NULL); // This can be 013 too.

	clientBase = (CreateInterfaceFn)GetProcAddress(GetModuleHandle("client.dll"), "CreateInterface");
	
	tfBaseClient = (IBaseClientDLL*)clientBase("VClient017", NULL);
	tfCEntList = (CEntList*)clientBase("VClientEntityList003", NULL);
	tfEntList = (IClientEntityList*)clientBase("VClientEntityList003", NULL);
	

	//Hook Some Functions I want.
	pDispatchUserMessageHook = new CVMTHookManager((DWORD**)tfBaseClient);
	oDispatchUserMessage = (tDispatchUserMessage)pDispatchUserMessageHook->dwHookMethod((DWORD)hkDispatchUserMessage, 36); // WAS 35, WORKING MAYBE.

	tfClientMode = **(IClientModeShared***)(FindPattern((DWORD)GetModuleHandle("client.dll"), 0xDE8000, (PBYTE)"\x83\xEC\x08\xA8\x01\x75\x1F\x83\xC8\x01", "xxxxxxxxxx") + 0x28);

	pCreateMoveHook = new CVMTHookManager((DWORD**)tfClientMode);
	oCreateMove = (tCreateMove)pCreateMoveHook->dwHookMethod((DWORD)hkCreateMove, 21);
}

HRESULT WINAPI startFunc(){
	HMODULE d3d = NULL;
	do{
		d3d = GetModuleHandle("d3d9.dll");
		Sleep(1000);
	} while (!d3d);
	DWORD* vtbl = 0;
	DWORD table = FindPattern((DWORD)GetModuleHandle("d3d9.dll"), 0x128000, (PBYTE)"\xC7\x06\x00\x00\x00\x00\x89\x86\x00\x00\x00\x00\x89\x86", "xx????xx????xx");

	memcpy(&vtbl, (void*)(table + 2), 4);
	EndSceneaddy = vtbl[42];
	resetAddy = vtbl[16];
	if (EndSceneaddy&&resetAddy&& vtbl[82])
	{
		oEndScene = (EndScene_)(DetourFunction((PBYTE)EndSceneaddy, (PBYTE)hkEndScene));
		oReset = (Reset_t)(DetourFunction((PBYTE)resetAddy, (PBYTE)hkReset));
	}
	hookSource();
	while (true){
		if (GetAsyncKeyState(VK_END) || forcedOff)
			break;
	}
	DetourFunction((PBYTE)vtbl[42], (PBYTE)oEndScene);
	DetourFunction((PBYTE)vtbl[16], (PBYTE)oReset);
	pDispatchUserMessageHook->UnHook();
	pCreateMoveHook->UnHook();

#if DEBUG_CONSOLE
	FreeConsole();
#endif

	FreeLibraryAndExitThread(ourModule, 0);
	//ExitThread(0);
	return 0;
}


BOOL WINAPI DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpvReserved){
	if (reason == DLL_PROCESS_ATTACH){
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)startFunc, NULL, NULL, NULL);
		ourModule = hInst;
#if DEBUG_CONSOLE
		AllocConsole();
		freopen("CONOUT$", "w", stdout);
#endif
	}
	return TRUE;
}
#pragma endregion