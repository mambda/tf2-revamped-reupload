#pragma once
#include <Windows.h>
#include "D3D.h"
#include "Vector.h"


#define MakePtr( Type, dwBase, dwOffset ) ( ( Type )( DWORD( dwBase ) + (DWORD)( dwOffset ) ) )
