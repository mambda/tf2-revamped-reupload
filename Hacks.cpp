#pragma once
#include "Hacks.h"
#include <string>

#define M_RADPI 57.295779513082f // (180 / pi)

void CalcAngle(const Vector& vSource, const Vector& vDest, Vector& vAngles)
{
	double delta[3] = { (vSource[0] - vDest[0]), (vSource[1] - vDest[1]), (vSource[2] - vDest[2]) };
	double hyp = sqrt(delta[0] * delta[0] + delta[1] * delta[1]);

	vAngles[0] = (float)(atan(delta[2] / hyp) * M_RADPI);
	vAngles[1] = (float)(atan(delta[1] / delta[0]) * M_RADPI);
	vAngles[2] = 0.0f;

	if (delta[0] >= 0.0f)
		vAngles[1] += 180.0f;

	NormalizeVector(vAngles);
	return;
}


void NormalizeVector(Vector &angle){
	if (angle.x > 89.0f)
	{
		angle.x = 89.0f;
	}

	if (angle.x < -89.0f){
		angle.x = -89.0f;
	}

	if (angle.y > 180.0f){
		angle.y -= 360.0f;
	}
	if (angle.y < -180.0f){
		angle.y += 360.0f;
	}
	angle.z = 0;
}

void ChangeName(char * name){
	tfSteamFriends->SetPersonaName(name);
}
